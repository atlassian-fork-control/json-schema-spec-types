# Json Schema Spec Types

Type definitions for the [json-schema specification draft-07](https://tools.ietf.org/html/draft-handrews-json-schema-01).

These type definitions were generated from https://raw.githubusercontent.com/json-schema-org/json-schema-spec/draft-07/schema.json using https://shakyshane.github.io/json-ts/ then cleaned up and refined.

To know more about the modifications applied to the original schema, check out the [CHANGELOG.md](CHANGELOG.md) file.
